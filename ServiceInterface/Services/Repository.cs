﻿using Microsoft.EntityFrameworkCore;
using SmsApi.BO;
using SmsApi.ControllerModels;
using SmsApi.Database.Models;
using SmsApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsApi.Services
{
    public class Repository : ISmsRepository
    {
        public Repository(SmsApi.Database.SmsDbContext smsDb)
        {
            SmsDb = smsDb;
        }

        protected SmsApi.Database.SmsDbContext SmsDb { get; }

        public async Task SaveSmsAsync(SmsBO data)
        {
            var model = new Sms()
            {
                Status = (int)data.Status,
                Senddate = data.Senddate,
                Mmc = data.Mmc,
                From = data.From,
                To = data.To,
                Message = data.Message,
                Id = data.Id
            };

            SmsDb.Sms.Add(model);

            await SmsDb.SaveChangesAsync();
        }

        public async Task<List<ControllerModels.CountriesResponse>> GetCountriesAsync()
        {
            var res = await SmsDb.Countries.Select(x =>
                                              new ControllerModels.CountriesResponse()
                                              {
                                                  Name = x.Name,
                                                  MMC = x.Mobilecode,
                                                  CC = x.Code,
                                                  PricePerSMS = x.PricePerSms
                                              })
                                              .ToListAsync();
            return res;
        }

        public async Task<ControllerModels.GetSentSmsResponse> GetSentSmsAsync(GetSentSms request)
        {
            var smsQuery = (IQueryable<Sms>)SmsDb.Sms.Include(s => s.MmcNavigation);

            if (request.DateTimeFrom.HasValue)
                smsQuery = smsQuery.Where(s => request.DateTimeFrom.Value <= s.Senddate);

            if (request.DateTimeTo.HasValue)
                smsQuery = smsQuery.Where(s => s.Senddate <= request.DateTimeTo.Value);

            var getQuery = smsQuery.OrderBy(x => x.Senddate)
                                   .Select(s => new
                                   {
                                       Data = new ControllerModels.SentSmsItem()
                                       {
                                           MMC = s.Mmc,
                                           From = s.From,
                                           To = s.To,
                                           State = (SmsState)s.Status,
                                           Price = s.MmcNavigation.PricePerSms
                                       },
                                       TotalCount = smsQuery.Count()
                                   });

            if (request.Skip.HasValue)
                getQuery = getQuery.Skip(request.Skip.Value);

            if (request.Take.HasValue)
                getQuery = getQuery.Take(request.Take.Value);

            var items = await getQuery.ToListAsync();

            var result = new GetSentSmsResponse()
            {
                TotalCount = items.FirstOrDefault()?.TotalCount ?? 0,
                Items = items.Select(x => x.Data).ToList()
            };

            return result;
        }

        /*
  * Parameters
- dateFrom: date [format: “yyyy-MM-dd”]
- dateTo: date [format: “yyyy-MM-dd”]
- mccList: string list [a list of mobile country codes to filter, e.g. “262,232”] o if list is
empty this means: include all mobile country codes
Returns
- array of statistic record
o day: date [format: “yyyy-MM-dd”]
o mcc: string [the mobile country code]
o pricePerSMS: decimal [the price per SMS in EUR, e.g. 0.06]
o count: integer[the count of SMS on the day and mcc]
o totalPrice: decimal [the total price of all SMS on the day and mcc in EUR, e.g.23.64]
         * */

        public async Task<List<StatisticsBO>> GetStatistic(Statistics request)
        {
            var query = SmsDb.Sms.Include(s => s.MmcNavigation)
                             .Select(x =>
                             new
                             {
                                 Mcc = x.Mmc,
                                 Day = x.Senddate.Date,
                                 x.MmcNavigation.PricePerSms
                             });

            if (request.DateFrom.HasValue)
                query = query.Where(x => request.DateFrom.Value.Date <= x.Day);

            if (request.DateTo.HasValue)
                query = query.Where(x => x.Day < request.DateTo.Value.Date);

            if (request.MccList?.Count > 0)
            {
                var mccList = request.MccList.Select(x => x.ToString());
                query = query.Where(x => mccList.Contains(x.Mcc));
            }

            var groupQuery = from item in query
                             group item by new { item.Day, item.Mcc, item.PricePerSms } into itemGroup

                             select new StatisticsBO
                             {
                                 Count = itemGroup.Count(),
                                 Day = itemGroup.Key.Day,
                                 Mcc = itemGroup.Key.Mcc,
                                 PricePerSMS = itemGroup.Key.PricePerSms,
                                 TotalPrice = itemGroup.Count() * itemGroup.Key.PricePerSms
                             };

            var items = await groupQuery.OrderBy(x => x.Day).ToListAsync();

            return items;
        }
    }
}