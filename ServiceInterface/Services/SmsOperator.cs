﻿using SmsApi.BO;
using SmsApi.ControllerModels;
using SmsApi.Interfaces;
using System.Threading.Tasks;

namespace SmsApi.Services
{
    public class SmsOperator : ISmsOperator
    {
        public async Task<SmsState> SendAsync(SmsBO sms)
        {
            return SmsState.Success;
        }
    }
}