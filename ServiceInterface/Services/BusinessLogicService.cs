﻿using ServiceStack;
using SmsApi.BO;
using SmsApi.ControllerModels;
using SmsApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SmsApi.Services
{
    public class BusinessLogicService : IBusinessLogicService
    {
        public BusinessLogicService(ISmsRepository repository, ISmsOperator smsOperator)
        {
            Repository = repository;
            SmsOperator = smsOperator;
        }

        protected ISmsRepository Repository { get; }
        protected ISmsOperator SmsOperator { get; }

        private static SemaphoreSlim dictionaryInitLock = new SemaphoreSlim(1, 1);
        private static IDictionary<string, string> _CctoMccDictionary;

        protected async ValueTask<IDictionary<string, string>> GetCountryCodesDictionary()
        {
            if (_CctoMccDictionary == null)
            {
                await dictionaryInitLock.WaitAsync(); //optimisation for very first calls

                if (_CctoMccDictionary == null) // check twice in case of other thread already fill it up
                {
                    var countries = await Repository.GetCountriesAsync();
                    _CctoMccDictionary = countries.ToImmutableDictionary(x => x.CC, x => x.MMC);

                    dictionaryInitLock.Release();
                }
            }

            return _CctoMccDictionary;
        }

        public Task<List<CountriesResponse>> GetCountriesAsync(Countries request)
        {
            return Repository.GetCountriesAsync();
        }

        public async Task<SendSmsResponse> SendSmsAsync(SendSms request)
        {
            request.From = request.From.Trim();
            request.To = request.To.Trim();
            var id = Guid.NewGuid();
            var result = new SendSmsResponse() { State = SmsState.InQueue };

            if (string.IsNullOrEmpty(request.From)) //country code not in dictionary
                throw new HttpError(result,
                                    HttpStatusCode.BadRequest,
                                    $"{nameof(request.From)} is empty",
                                     $"{ nameof(request.From) } is empty");
            if (string.IsNullOrEmpty(request.To)) //country code not in dictionary
                throw new HttpError(result,
                                    HttpStatusCode.BadRequest,
                                    $"{nameof(request.To)} is empty",
                                     $"{nameof(request.To) } is empty");

            var mmc = await GetMMC(request.To);

            if (string.IsNullOrEmpty(mmc)) //country code not in dictionary
                throw new HttpError(result,
                                    HttpStatusCode.BadRequest,
                                    "Unsupported country code",
                                    "Unsupported country code");

            var sms = new SmsBO()
            {
                Senddate = DateTime.UtcNow,
                Mmc = mmc,
                From = request.From,
                To = request.To,
                Message = request.Text,
                Id = id,
                Status = SmsState.InQueue
            };

            sms.Status = await SmsOperator.SendAsync(sms);

            await Repository.SaveSmsAsync(sms);

            result.State = sms.Status;

            return result;
        }

        public async Task<GetSentSmsResponse> GetSentSmsAsync(GetSentSms request)
        {
            return await Repository.GetSentSmsAsync(request);
        }

        public async Task<List<StatisticsResponse>> GetStatistic(Statistics request)
        {
            var statistics = await Repository.GetStatistic(request);
            return statistics.Select(x => new StatisticsResponse()
            {
                Day = x.Day.ToString("yyyy-MM-dd"),
                Mcc = x.Mcc,
                Count = x.Count,
                PricePerSMS = x.PricePerSMS,
                TotalPrice = x.TotalPrice
            }).ToList();
        }

        /// <summary>
        /// Separate MMC code from recepient number
        /// </summary>
        /// <param name="recepientNumber">Number which will receive sms</param>
        /// <returns>Mobile country code as string</returns>
        protected async ValueTask<string> GetMMC(string recepientNumber)
        {
            const char plusPrefix = '+';
            const int maxCountryCodeLength = 5;

            var dictionary = await GetCountryCodesDictionary();

            int codeLength = 2;
            int startIndex = 0;

            if (recepientNumber.First() == plusPrefix)
                startIndex++;

            string mmc;
            string numberSubstring;

            do
            {
                numberSubstring = recepientNumber.Substring(startIndex, codeLength);
                codeLength++;
            }
            while (!dictionary.TryGetValue(numberSubstring, out mmc) && numberSubstring.Length <= maxCountryCodeLength);

            return mmc;
        }
    }
}