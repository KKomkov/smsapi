﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SmsApi.Database.Migrations
{
    public partial class SeedCountries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO countries (code,""name"",mobilecode,price_per_sms) VALUES
                  ('380', 'Ukraine', '255', 0.030)
                , ('49', 'Germany', '262', 0.055)
                , ('43', 'Austria', '232', 0.053)
                , ('48', 'Poland', '260', 0.032);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("Delete from  countries;");
        }
    }
}