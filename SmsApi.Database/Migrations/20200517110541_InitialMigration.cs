﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace SmsApi.Database.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "countries",
                columns: table => new
                {
                    mobilecode = table.Column<string>(maxLength: 100, nullable: false),
                    code = table.Column<string>(maxLength: 100, nullable: false),
                    name = table.Column<string>(maxLength: 200, nullable: false),
                    price_per_sms = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("countries_pk", x => x.mobilecode);
                });

            migrationBuilder.CreateTable(
                name: "sms",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    from = table.Column<string>(maxLength: 100, nullable: false),
                    to = table.Column<string>(maxLength: 100, nullable: false),
                    message = table.Column<string>(nullable: false),
                    mmc = table.Column<string>(maxLength: 100, nullable: false),
                    senddate = table.Column<DateTime>(nullable: false),
                    status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sms", x => x.id);
                    table.ForeignKey(
                        name: "sms_fk",
                        column: x => x.mmc,
                        principalTable: "countries",
                        principalColumn: "mobilecode",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "countries_code_un",
                table: "countries",
                column: "code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "countries_name_un",
                table: "countries",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_sms_mmc",
                table: "sms",
                column: "mmc");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "sms");

            migrationBuilder.DropTable(
                name: "countries");
        }
    }
}