using Funq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceStack;
using SmsApi.Database;
using SmsApi.Interfaces;
using SmsApi.Services;

namespace SmsApi
{
    public class Startup : ModularStartup
    {
        private const string dbConnection = "ConnectionStrings:SmsDatabase";

        public Startup(IConfiguration configuration)
        {
            Configuration = new ConfigurationBuilder()
                                .AddConfiguration(configuration)
                                .Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public new void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IBusinessLogicService, BusinessLogicService>();
            services.AddScoped<ISmsRepository, Repository>();
            services.AddScoped<ISmsOperator, SmsOperator>();

            IServiceCollection serviceCollection = services.AddDbContext<SmsDbContext>(
            options =>
            {
                options.UseNpgsql(Configuration.GetValue<string>(dbConnection));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseServiceStack(new AppHost
            {
                AppSettings = new NetCoreAppSettings(Configuration)
            });

            using (var serviceScope = app.ApplicationServices
           .GetRequiredService<IServiceScopeFactory>()
           .CreateScope())
            {
                using (var dbcontext = serviceScope.ServiceProvider.GetService<SmsDbContext>())
                {
                    if (dbcontext.Database.CanConnect())
                    {
                        dbcontext.Database.Migrate();
                    }
                    else
                        throw new System.Exception("Failed connect to Server database");
                }
            }
        }
    }

    public class AppHost : AppHostBase
    {
        public AppHost() : base("SmsApi", typeof(SmsApi.Controlers.SmsController).Assembly)
        {
        }

        // Configure your AppHost with the necessary configuration and dependencies your App needs
        public override void Configure(Container container)
        {
            SetConfig(new HostConfig
            {
                DefaultRedirectPath = "/metadata",
                DebugMode = AppSettings.Get(nameof(HostConfig.DebugMode), false)
            });
        }
    }
}