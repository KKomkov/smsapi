﻿using System;

namespace SmsApi.BO
{
    public class StatisticsBO
    {
        public DateTime Day { get; set; }
        public string Mcc { get; set; }
        public decimal PricePerSMS { get; set; }
        public int Count { get; set; }
        public decimal TotalPrice { get; set; }
    }
}