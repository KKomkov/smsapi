﻿using SmsApi.ControllerModels;
using System;

namespace SmsApi.BO
{
    public class SmsBO
    {
        public SmsState Status { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Message { get; set; }
        public string Mmc { get; set; }
        public DateTime Senddate { get; set; }
        public Guid Id { get; set; }
    }
}