﻿using ServiceStack;
using System;
using System.Collections.Generic;

namespace SmsApi.ControllerModels
{
    /*
     Service::GetStatistics
Parameters
- dateFrom: date [format: “yyyy-MM-dd”]
- dateTo: date [format: “yyyy-MM-dd”]
- mccList: string list [a list of mobile country codes to filter, e.g. “262,232”] o if list is
empty this means: include all mobile country codes
Returns
- array of statistic record
o day: date [format: “yyyy-MM-dd”]
o mcc: string [the mobile country code]
o pricePerSMS: decimal [the price per SMS in EUR, e.g. 0.06]
o count: integer[the count of SMS on the day and mcc]
o totalPrice: decimal [the total price of all SMS on the day and mcc in EUR, e.g.23.64]
URL format
- GET /statistics.json?dateFrom=2018-03-01&dateTo=2018-03-05&mccList=262,232
- GET /statistics.xml?dateFrom=2018-03-01&dateTo=2018-03-05&mccList=262,232
         */

    [Route("/statistics", "GET")]
    public class Statistics : IReturn<List<CountriesResponse>>
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public List<int> MccList { get; set; }
    }

    public class StatisticsResponse
    {
        public string Day { get; set; }
        public string Mcc { get; set; }
        public decimal PricePerSMS { get; set; }
        public int Count { get; set; }
        public decimal TotalPrice { get; set; }
    }
}