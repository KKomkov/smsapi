﻿using ServiceStack;
using System;
using System.Collections.Generic;

namespace SmsApi.ControllerModels
{
    /*
    Service::GetSentSMS
 Parameters
 - dateTimeFrom: dateTime [format: “yyyy-MM-ddTHH:mm:ss”, UTC]
 - dateTimeTo: dateTime [format: “yyyy-MM-ddTHH:mm:ss”, UTC]
 - skip: integer [skip n records]
 - take: integer [take n records]
 Returns
 - totalCount: int [the total count of all items matching the filter]
 - items: array of SMS o dateTime: dateTime [the date and time the SMS was sent, format:
 “yyyy-MMddTHH:mm:ss, UTC]
 o mcc: string [the mobile country code of the country  where the receiver of the SMS belongs to]
 o from: string [the sender of the SMS] o to: string [the receiver of the SMS]
 o price: decimal [the price of the SMS in EUR, e.g. 0.06]
 o state: enum (Success, Failed)

URL format
- GET /sms/sent.json?dateTimeFrom=2018-03-01T11:30:20&dateTimeTo=2018-0302T09:20:22&skip=100&take=50
- GET /sms/sent.xml?dateTimeFrom=2018-03-01T11:30:20&dateTimeTo=2018-0302T09:20:22&skip=100&take=50

    */

    [Route("/sms/sent/{DateTimeFrom}/{DateTimeTo}/{Skip}/{Take}", "GET")]
    [Route("/sms/sent", "GET")]
    public class GetSentSms : IReturn<GetSentSmsResponse> //a bit different naming to avoid mix with SendSms
    {
        public DateTime? DateTimeFrom { get; set; }
        public DateTime? DateTimeTo { get; set; }
        public int? Skip { get; set; }
        public int? Take { get; set; }
    }

    public class GetSentSmsResponse
    {
        public int TotalCount { get; set; }
        public List<SentSmsItem> Items { get; set; }
    }

    public class SentSmsItem
    {
        public string MMC { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public decimal Price { get; set; }
        public SmsState State { get; set; }
    }
}