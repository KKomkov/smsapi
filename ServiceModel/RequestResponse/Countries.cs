﻿using ServiceStack;
using System.Collections.Generic;

namespace SmsApi.ControllerModels
{
    /*
    Service::GetCountries
Parameters
- None
Returns
- array of country
o mcc: string[the mobile country code of the country, e.g.“262” for Germany]
o cc: string[the country code of the country, e.g. “49” for Germany]
o name: string[the name of the country, e.g. “Germany”]
o pricePerSMS: decimal[the price per SMS sent in this country in EUR, e.g. 0.06]

URL format
- GET /countries.json
- GET /countries.xml
        */

    [Route("/countries", "GET")]
    public class Countries : IReturn<List<CountriesResponse>>
    {
    }

    public class CountriesResponse
    {
        public string MMC { get; set; }
        public string CC { get; set; }
        public string Name { get; set; }
        public decimal PricePerSMS { get; set; }
    }
}