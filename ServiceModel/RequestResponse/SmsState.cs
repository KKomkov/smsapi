﻿namespace SmsApi.ControllerModels
{
    public enum SmsState
    {
        Failed = 0,
        Success,
        InQueue
    }
}