﻿using ServiceStack;

namespace SmsApi.ControllerModels
{
    /*
 Service::SendSMS
Parameters
- from: string [the sender of the message]
- to: string [the receiver of the message]
- text: string [the text which should be sent]
Returns
- state: enum (Success, Failed)
URL format
- GET /sms/send.json?from=The+Sender&to=%2B4917421293388&text=Hello+World
- GET /sms/send.xml?from=The+Sender&to=%2B4917421293388&text=Hello+World
*/

    [Route("/sms/send/{From}/{To}/{Text}", "GET")]
    [Route("/sms/send", "GET")] // Request change system state so it must be POST
    [Route("/sms/send", "POST")]
    public class SendSms : IReturn<SendSmsResponse>
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Text { get; set; }
    }

    public class SendSmsResponse
    {
        public SmsState State { get; set; }
    }
}