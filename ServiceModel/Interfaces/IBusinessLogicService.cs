﻿using SmsApi.ControllerModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmsApi.Interfaces
{
    public interface IBusinessLogicService
    {
        Task<SendSmsResponse> SendSmsAsync(SendSms request);

        Task<GetSentSmsResponse> GetSentSmsAsync(GetSentSms request);

        Task<List<CountriesResponse>> GetCountriesAsync(Countries request);

        Task<List<StatisticsResponse>> GetStatistic(Statistics request);
    }
}