﻿using SmsApi.BO;
using SmsApi.ControllerModels;
using System.Threading.Tasks;

namespace SmsApi.Interfaces
{
    public interface ISmsOperator
    {
        Task<SmsState> SendAsync(SmsBO sms);
    }
}