﻿using SmsApi.BO;
using SmsApi.ControllerModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmsApi.Interfaces
{
    public interface ISmsRepository
    {
        Task SaveSmsAsync(SmsBO data);

        Task<List<CountriesResponse>> GetCountriesAsync();

        Task<GetSentSmsResponse> GetSentSmsAsync(GetSentSms request);

        Task<List<StatisticsBO>> GetStatistic(Statistics request);
    }
}