﻿using ServiceStack;
using SmsApi.ControllerModels;
using SmsApi.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmsApi.Controlers
{
    public class SmsController : Service
    {
        public SmsController(IBusinessLogicService businessLogicService)
        {
            Service = businessLogicService;
        }

        protected IBusinessLogicService Service { get; set; }

        public Task<SendSmsResponse> Any(SendSms request)
        {
            return Service.SendSmsAsync(request);
        }

        public Task<GetSentSmsResponse> Any(GetSentSms request)
        {
            return Service.GetSentSmsAsync(request);
        }

        public Task<List<CountriesResponse>> Any(Countries request)
        {
            return Service.GetCountriesAsync(request);
        }

        public Task<List<StatisticsResponse>> Any(Statistics request)
        {
            return Service.GetStatistic(request);
        }
    }
}