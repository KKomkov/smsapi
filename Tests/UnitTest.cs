﻿//using NUnit.Framework;
//using ServiceStack;
//using ServiceStack.Testing;
//using SmsApi.ServiceModel;
//using System.Threading.Tasks;

//namespace SmsApi.Tests
//{
//    public class UnitTest
//    {
//        private readonly ServiceStackHost appHost;

//        public UnitTest()
//        {
//            appHost = new BasicAppHost().Init();
//            appHost.Container.AddTransient<SmsServices>();
//        }

//        [OneTimeTearDown]
//        public void OneTimeTearDown() => appHost.Dispose();

//        [Test]
//        public async Task Can_call_MyServices()
//        {
//            var service = appHost.Container.Resolve<SmsServices>();

//            var response = await service.Any(new Hello { Name = "World" });

//            Assert.That(response.Result, Is.EqualTo("Hello, World!"));
//        }
//    }
//}